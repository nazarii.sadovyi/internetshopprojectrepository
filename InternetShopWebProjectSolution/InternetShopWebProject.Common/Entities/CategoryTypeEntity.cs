﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.Common.Entities
{
    public class CategoryTypeEntity
    {
        public List<StickerType> Types = new List<StickerType>();
        public List<StickerCategoty> Categories = new List<StickerCategoty>();
        public CategoryTypeEntity(IEnumerable<StickerType> types, IEnumerable<StickerCategoty> categoties)
        {
            Types.AddRange(types);
            Categories.AddRange(categoties);
        }
    }
}
