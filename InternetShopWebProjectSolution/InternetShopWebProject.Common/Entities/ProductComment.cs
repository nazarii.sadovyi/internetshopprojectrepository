﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.Common.Entities
{
    [Table("ProductComment")]
    public class ProductComment
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Text { get; set; }
    }
}
