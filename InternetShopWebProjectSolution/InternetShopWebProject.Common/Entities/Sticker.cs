﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetShopWebProject.Common.Entities
{
    [Table("Stickers")]
    public class Sticker
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Height { get; set; }
        public int Wight { get; set; }
        public float Price { get; set; }
        public Guid CategotyId { get; set; }
        public Guid TypeId { get; set; }
        public float Rating { get; set; }
        public DateTime Added { get; set; }
        public string Image { get; set; }
        public bool Available { get; set; }
    }
}
