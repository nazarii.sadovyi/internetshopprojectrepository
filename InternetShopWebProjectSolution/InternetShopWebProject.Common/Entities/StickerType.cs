﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InternetShopWebProject.Common.Entities
{
    public class StickerType
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
