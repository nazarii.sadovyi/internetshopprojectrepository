﻿using InternetShopWebProject.Common.Entities;

namespace InternetShopWebProject.Common.Models
{
    public class StickerCountModel
    {
        public Sticker Key { get; set; }
        public int Value { get; set; }
        public StickerCountModel()
        {

        }
        public StickerCountModel(Sticker sticker, int count)
        {
            Key = sticker;
            Value = count;
        }
    }
}