﻿namespace InternetShopWebProject.Common.Models
{
    public class NewComment
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
