﻿using System;

namespace InternetShopWebProject.Common.Models
{
    public class SelectedMenuItem
    {
        public Guid? SelectedTypeId;
        public Guid? SelectedCategoryId;
    }
}