﻿using InternetShopWebProject.Common.Entities;
using System.Collections.Generic;

namespace InternetShopWebProject.Common.Models
{
    public class ShoppingBagModel
    {
        public List<StickerCountModel> StickersToOrder { get; set; } = new List<StickerCountModel>();
        public float TotalPrice { get; set; } = 0;
        public ShoppingBagModel()
        {

        }
        public ShoppingBagModel(IEnumerable<Sticker> stickers)
        {
            foreach (var sticker in stickers)
            {
                StickersToOrder.Add(new StickerCountModel(sticker, 1));
                TotalPrice += sticker.Price;
            }
        }
    }
}