﻿using InternetShopWebProject.Common.Entities;
using System;

namespace InternetShopWebProject.Common.Models
{
    public class ProductModel: MainProductModel
    {
        public string Size { get; set; }
        public bool Available { get; set; }

        public ProductModel ToProductModel(Sticker sticker)
        {
            return new ProductModel
            {
                Id = sticker.Id,
                Name = sticker.Name,
                Size = $"Розмір {sticker.Height} х {sticker.Wight} см",
                Available = sticker.Available,
                Image = string.IsNullOrEmpty(sticker.Image) ? _defaultImage : sticker.Image,
                Price = $"{sticker.Price} грн"
            };
        }
    }
}
