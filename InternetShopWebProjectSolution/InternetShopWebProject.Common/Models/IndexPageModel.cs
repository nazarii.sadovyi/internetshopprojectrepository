﻿using System;
using System.Collections.Generic;

namespace InternetShopWebProject.Common.Models
{
    public class IndexPageModel
    {
        public List<ProductWithBagInfo> Stickers = new List<ProductWithBagInfo>();
        public SelectedMenuItem SelectedMenu { get; set; }
        public int TotalPages { get; private set; }
        public int Page { get; private set; }

        public IndexPageModel(IEnumerable<MainProductModel> stickers, IList<Guid> coockiesProducs,
            Guid? selectedType, Guid? selectedCategory, int page, int itemPerPage, int count)
        {
            TotalPages = count / itemPerPage;
            Page = page;
            SelectedMenu = new SelectedMenuItem()
            {
                SelectedCategoryId = selectedCategory,
                SelectedTypeId = selectedType
            };
            foreach (var item in stickers)
            {
                var isInBag = false;
                if (coockiesProducs.Contains(item.Id))
                {
                    isInBag = true;
                }
                Stickers.Add(new ProductWithBagInfo(item, isInBag));
            }
        }

    }
}