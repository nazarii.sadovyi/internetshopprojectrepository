﻿namespace InternetShopWebProject.Common.Models
{
    public class ProductWithBagInfo
    {
        public ProductWithBagInfo(MainProductModel key, bool value)
        {
            Key = key;
            Value = value;
        }
        public MainProductModel Key { get; set; }
        public bool Value { get; set; }
    }
}
