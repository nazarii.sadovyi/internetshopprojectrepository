﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;

namespace InternetShopWebProject.Common.Models
{
    public class CardInfo
    {
        public Guid Id { get; set; }
        public ProductModel Sticker { get; set; }
        public IEnumerable<ProductComment> Comments { get; set; }
        public NewComment NewComment { get; set; }
        public bool InBag { get; set; }
        public bool NewCommentAdded { get; set; }
    }
}
