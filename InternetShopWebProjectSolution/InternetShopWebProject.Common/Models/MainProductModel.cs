﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.Common.Models
{
    public class MainProductModel
    {
        protected readonly string _defaultImage = "";
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Image { get; set; }

        public MainProductModel ToMainProductModel(Sticker sticker)
        {
            return new MainProductModel
            {
                Id = sticker.Id,
                Name = sticker.Name,
                Image = string.IsNullOrEmpty(sticker.Image) ? _defaultImage : sticker.Image,
                Price = $"{sticker.Price} грн"
            };
        }
    }
}
