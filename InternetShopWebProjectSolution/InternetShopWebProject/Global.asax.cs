﻿using InternetShopWebProject.UnityFactory;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity;
using Unity.Mvc5;

namespace InternetShopWebProject
{
    public class MvcApplication : HttpApplication, IContainerAccessor
    {
        private static IUnityContainer _container;
        public static IUnityContainer Container
        {
            get
            {
                return _container;
            }
        }

        IUnityContainer IContainerAccessor.Container
        {
            get { return Container; }
        }

        protected void Application_Start()
        {
            ControllerBuilder.Current.SetControllerFactory(typeof(UnityControllerFactory));
            InitContainer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            CleanUp();
        }


        private static void InitContainer()
        {
            if (_container == null)
            {
                _container = new UnityContainer();
            }
            DependencyResolver.SetResolver(new UnityDependencyResolver(_container));
            _container.RegisterComponents();
        }

        private static void CleanUp()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }
    }
}
