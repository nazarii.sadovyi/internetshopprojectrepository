﻿using InternetShopWebProject.Common.Models;
using InternetShopWebProject.CookiesManager;
using InternetShopWebProject.DAL.Repository;
using InternetShopWebProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity.Attributes;

namespace InternetShopWebProject.Controllers
{
    public class ShoppingBagController : Controller
    {
        [Dependency]
        public IStickerRepository _stickerRepos { get; set; }

        public ActionResult Index()
        {
            var stickerIdsInBag = ShoppingCart.Products;
            var stickers = _stickerRepos.StickersById(stickerIdsInBag);
            var model = new ShoppingBagModel(stickers);
            return View(model);
        }

        public ActionResult AddToShoppingBag(Guid id, string returnUrl)
        {
            ShoppingCart.Add(id);
            return Redirect(returnUrl);
        }

        public ActionResult RemoveFromShoppingBag(Guid id, string returnUrl)
        {
            ShoppingCart.Remove(id);
            return Redirect(returnUrl);
        }

        public ActionResult ConfirmOrder(ShoppingBagModel model)
        {
            
            return View("Index", model);
        }

        public ActionResult RemoveProduct(ShoppingBagModel model)
        {

            return View("Index", model);
        }
    }
}