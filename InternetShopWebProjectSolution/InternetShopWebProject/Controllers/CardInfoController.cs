﻿using InternetShopWebProject.Common.Models;
using InternetShopWebProject.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Unity.Attributes;
using System.Web.Mvc;
using InternetShopWebProject.Common.Entities;
using InternetShopWebProject.Models;
using InternetShopWebProject.CookiesManager;

namespace InternetShopWebProject.Controllers
{
    public class CardInfoController : Controller
    {
        [Dependency]
        public IStickerRepository _stickerRepos { get; set; }
        [Dependency]
        public ICommentsRepository _commentsRepository { get; set; }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Product(Guid id, bool wasCommentAdd = false)
        {
            var model = new CardInfo();
            model.Id = id;
            model.Sticker = new ProductModel().ToProductModel(_stickerRepos.StickerInfo(id));
            model.Comments = _commentsRepository.GetComments(id).OrderByDescending(x => x.CreatedDate);
            model.InBag = ShoppingCart.IsInBag(id);
            model.NewCommentAdded = wasCommentAdd;
            LastViewed.Add(id);
            return View(model);
        }

        public ActionResult NewComment(CardInfo cardInfo)
        {
            var userId = User.Identity.GetUserId();
            _commentsRepository.NewComment(new ProductComment
            {
                CreatedDate = DateTime.Now,
                Id = Guid.NewGuid(),
                ProductId = cardInfo.Id,
                Text = cardInfo.NewComment.Text,
                UserName = cardInfo.NewComment.Name != null ? cardInfo.NewComment.Name : User.Identity.Name,
                UserId = userId != null ? new Guid(userId) : (Guid?)null
            });
            return RedirectToAction("Product", new { id = cardInfo.Id, wasCommentAdd = true });
        }
    }
}