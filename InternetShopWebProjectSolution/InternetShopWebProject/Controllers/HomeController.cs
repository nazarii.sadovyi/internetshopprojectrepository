﻿using InternetShopWebProject.Common.Models;
using InternetShopWebProject.CookiesManager;
using InternetShopWebProject.DAL.Repository;
using System;
using System.Linq;
using System.Web.Mvc;
using Unity.Attributes;

namespace InternetShopWebProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly int _itemsPerPage = 8;
        [Dependency]
        public IStickerRepository _stickerRepos { get; set; }

        public ActionResult Index(Guid? typeId, Guid? categoryId, int page = 0)
        {
            var stickersCount = _stickerRepos.AllSticker().Count();
            var sticker = _stickerRepos.AllSticker()
                .Where(s => typeId == null || s.TypeId == typeId)
                .Where(s => categoryId == null || s.CategotyId == categoryId)
                .OrderByDescending(g => g.Added).Skip(page * _itemsPerPage).Take(_itemsPerPage)
                .ToList()
                .Select(p => new MainProductModel().ToMainProductModel(p));
            return View(new IndexPageModel(sticker, ShoppingCart.Products, typeId, categoryId, 
                page, _itemsPerPage, stickersCount));
        }
    }
}