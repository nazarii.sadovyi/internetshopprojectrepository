﻿using Unity;

namespace InternetShopWebProject.UnityFactory
{
    internal interface IContainerAccessor
    {
        IUnityContainer Container { get; }
    }
}