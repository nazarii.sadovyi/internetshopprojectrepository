﻿using InternetShopWebProject.Controllers;
using InternetShopWebProject.DAL.Repository;
using InternetShopWebProject.DAL.UnitOfWork;
using InternetShopWebProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.Injection;

namespace InternetShopWebProject.UnityFactory
{
    public static class InitUnityType
    {
        public static void RegisterComponents(this IUnityContainer container)
        {
            container.RegisterType<IStickerCategoryRepository, StickerCategoryRepository>();
            container.RegisterType<IStickerRepository, StickerRepository>();
            container.RegisterType<IStickerTypeRepository, StickerTypeRepository>();
            container.RegisterType<ICommentsRepository, CommentsRepository>();
            container.RegisterType<ITypeCategory, CategoryTypeUoW>();

            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<UserManager<ApplicationUser>>();
            container.RegisterType<DbContext, ApplicationDbContext>();
            container.RegisterType<ApplicationUserManager>();
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(x => HttpContext.Current.GetOwinContext().Authentication));
        }
    }
}