﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetShopWebProject.CookiesManager
{
    public static class ShoppingCart
    {
        private readonly static string ShoppingCartKey = "ShoppingCart";
        private readonly static CookiesBase _cookiesBase;
        public static IList<Guid> Products
        {
            get { return _cookiesBase.Items; }
        }
        static ShoppingCart()
        {
            _cookiesBase = new CookiesBase(ShoppingCartKey);
        }
        public static bool IsInBag(Guid id)
        {
            return _cookiesBase.Items.Contains(id);
        }

        public static void Add(Guid id)
        {
            _cookiesBase.Add(id);
        }
        public static void Remove(Guid id)
        {
            _cookiesBase.Remove(id);
        }
    }
}