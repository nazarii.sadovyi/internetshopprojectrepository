﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetShopWebProject.CookiesManager
{
    public class CookiesBase
    {
        private readonly string _key;
        public CookiesBase(string key)
        {
            _key = key;
        }
        public IList<Guid> Items
        {
            get
            {
                var items = HttpContext.Current.Request.Cookies[_key];
                if (items == null)
                {
                    HttpContext.Current.Request.Cookies.Add(new HttpCookie(_key, string.Empty));
                    return new List<Guid>();
                }
                if (items.Value == string.Empty)
                {
                    return new List<Guid>();
                }
                return items.Value.Split(',').Select(id => new Guid(id)).ToList();
            }
            private set
            {
                HttpContext.Current.Request.Cookies.Remove(_key);
                HttpContext.Current.Request.Cookies.Add(new HttpCookie(_key, string.Join(",", value)));
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(_key, string.Join(",", value)));
            }
        }

        public void Add(Guid id)
        {
            Update(x => x.Add(id));
        }
        public void Remove(Guid id)
        {
            Update(x => x.Remove(id));
        }

        private void Update(Action<ICollection<Guid>> func)
        {
            var item = Items;
            func(item);
            Items = item;
        }
    }
}