﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetShopWebProject.CookiesManager
{
    public static class LastViewed
    {
        private readonly static string LastViewedProductsKey = "LastViewedProducts";
        private readonly static int MaxProductsCount = 12;
        private readonly static CookiesBase _cookiesBase;
        public static IList<Guid> Products
        {
            get { return _cookiesBase.Items; }
        }
        static LastViewed()
        {
            _cookiesBase = new CookiesBase(LastViewedProductsKey);
        }

        public static void Add(Guid id)
        {
            if (_cookiesBase.Items.Contains(id))
            {
                _cookiesBase.Remove(id);
            }
            _cookiesBase.Add(id);
            if (_cookiesBase.Items.Count > MaxProductsCount )
            {
                _cookiesBase.Remove(_cookiesBase.Items[0]);
            }
        }
    }
}