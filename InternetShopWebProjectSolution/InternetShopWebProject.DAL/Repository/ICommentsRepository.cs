﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.DAL.Repository
{
    public interface ICommentsRepository
    {
        IEnumerable<ProductComment> GetComments(Guid productId);
        Task NewComment(ProductComment comment);
    }
}
