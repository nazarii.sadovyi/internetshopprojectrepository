﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetShopWebProject.Common.Entities;

namespace InternetShopWebProject.DAL.Repository
{
    public class StickerRepository : IStickerRepository
    {
        private readonly AppDbContext _context = new AppDbContext();

        public async Task AddSticker(Sticker sticker)
        {
            _context.Stickers.Add(sticker);
            await _context.SaveChangesAsync();
        }

        public IQueryable<Sticker> AllSticker()
        {
            return _context.Stickers.AsQueryable();
        }

        public IQueryable<Sticker> StickersById(IEnumerable<Guid> ids)
        {
            return _context.Stickers.AsQueryable().Where(s => ids.Contains(s.Id));
        }

        public Sticker StickerInfo(Guid id)
        {
            return _context.Stickers.FirstOrDefault(s => s.Id == id);
        }
    }
}
