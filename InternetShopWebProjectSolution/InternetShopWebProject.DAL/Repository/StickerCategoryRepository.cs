﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetShopWebProject.Common.Entities;

namespace InternetShopWebProject.DAL.Repository
{
    public class StickerCategoryRepository : IStickerCategoryRepository
    {
        private readonly AppDbContext _context = new AppDbContext();

        public async Task AddCategory(StickerCategoty category)
        {
            var result = _context.StickerCategoties.FirstOrDefault(c => c.Name == category.Name);
            if (result == null)
            {
                _context.StickerCategoties.Add(category);
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<StickerCategoty> AllCategory()
        {
            return _context.StickerCategoties;
        }

    }
}
