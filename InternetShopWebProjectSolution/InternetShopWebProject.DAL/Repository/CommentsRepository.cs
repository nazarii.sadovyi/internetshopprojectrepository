﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetShopWebProject.Common.Entities;

namespace InternetShopWebProject.DAL.Repository
{
    public class CommentsRepository: ICommentsRepository
    {
        private readonly AppDbContext _context = new AppDbContext();

        public IEnumerable<ProductComment> GetComments(Guid productId)
        {
            return _context.ProductComments.Where(p => p.ProductId == productId);
        }

        public async Task NewComment(ProductComment comment)
        {
            _context.ProductComments.Add(comment);
            await _context.SaveChangesAsync();
        }
    }
}
