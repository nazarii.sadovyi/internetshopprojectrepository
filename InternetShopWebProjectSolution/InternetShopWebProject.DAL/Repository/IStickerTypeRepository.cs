﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.DAL.Repository
{
    public interface IStickerTypeRepository
    {
        IEnumerable<StickerType> AllType();
        Task AddType(StickerType type);
    }
}
