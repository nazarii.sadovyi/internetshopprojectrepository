﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetShopWebProject.Common.Entities;

namespace InternetShopWebProject.DAL.Repository
{
    public class StickerTypeRepository : IStickerTypeRepository
    {
        private readonly AppDbContext _context = new AppDbContext();
        public async Task AddType(StickerType type)
        {
            var result = _context.StickerTypes.FirstOrDefault(c => c.Name == type.Name);
            if (result == null)
            {
                _context.StickerTypes.Add(type);
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<StickerType> AllType()
        {
            return _context.StickerTypes;
        }
    }
}
