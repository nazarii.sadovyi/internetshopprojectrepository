﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.DAL.Repository
{
    public interface IStickerCategoryRepository
    {
        IEnumerable<StickerCategoty> AllCategory();
        Task AddCategory(StickerCategoty category); 
    }
}
