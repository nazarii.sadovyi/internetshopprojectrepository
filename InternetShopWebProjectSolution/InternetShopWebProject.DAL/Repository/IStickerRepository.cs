﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.DAL.Repository
{
    public interface IStickerRepository
    {
        IQueryable<Sticker> AllSticker();
        IQueryable<Sticker> StickersById(IEnumerable<Guid> ids);
        Sticker StickerInfo(Guid id);
        Task AddSticker(Sticker sticker);
    }
}
