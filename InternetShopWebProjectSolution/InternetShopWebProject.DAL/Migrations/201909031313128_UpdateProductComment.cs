﻿namespace InternetShopWebProject.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComment", "UserName", c => c.String());
            AlterColumn("dbo.ProductComment", "UserId", c => c.Guid());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductComment", "UserId", c => c.Guid(nullable: false));
            DropColumn("dbo.ProductComment", "UserName");
        }
    }
}
