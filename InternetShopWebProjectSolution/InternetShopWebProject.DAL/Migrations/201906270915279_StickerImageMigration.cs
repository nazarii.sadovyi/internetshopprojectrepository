﻿namespace InternetShopWebProject.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StickerImageMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stickers", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stickers", "Image");
        }
    }
}
