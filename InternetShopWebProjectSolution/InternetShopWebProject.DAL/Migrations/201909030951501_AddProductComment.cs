﻿namespace InternetShopWebProject.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductComment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductComment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProductComment");
        }
    }
}
