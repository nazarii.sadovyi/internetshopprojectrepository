﻿namespace InternetShopWebProject.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StickerCategoties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stickers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Height = c.Int(nullable: false),
                        Wight = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        CategotyId = c.Guid(nullable: false),
                        TypeId = c.Guid(nullable: false),
                        Rating = c.Single(nullable: false),
                        Added = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StickerTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StickerTypes");
            DropTable("dbo.Stickers");
            DropTable("dbo.StickerCategoties");
        }
    }
}
