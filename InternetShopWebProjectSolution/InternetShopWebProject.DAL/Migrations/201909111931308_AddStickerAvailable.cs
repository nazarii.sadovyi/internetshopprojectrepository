﻿namespace InternetShopWebProject.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStickerAvailable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stickers", "Available", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stickers", "Available");
        }
    }
}
