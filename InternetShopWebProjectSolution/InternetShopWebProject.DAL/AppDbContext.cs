﻿using InternetShopWebProject.Common.Entities;
using System.Data.Entity;

namespace InternetShopWebProject.DAL
{
    public class AppDbContext: DbContext
    {
        public DbSet<Sticker> Stickers { get; set; }
        public DbSet<StickerCategoty> StickerCategoties { get; set; }
        public DbSet<StickerType> StickerTypes { get; set; }
        public DbSet<ProductComment> ProductComments { get; set; }
        public AppDbContext() : base("aspnet-InternetShopWebProject-f8786029-d4d2-414c-bf34-0cffd16fb74b")
        {

        }
    }
}
