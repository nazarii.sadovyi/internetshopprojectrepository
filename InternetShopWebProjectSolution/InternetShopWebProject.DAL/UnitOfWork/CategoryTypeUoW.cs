﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetShopWebProject.Common.Entities;
using InternetShopWebProject.DAL.Repository;

namespace InternetShopWebProject.DAL.UnitOfWork
{
    public class CategoryTypeUoW : ITypeCategory
    {
        private IStickerCategoryRepository _categoryRepos { get; set; }
        private IStickerTypeRepository _typeRepos { get; set; }
        public CategoryTypeUoW(IStickerCategoryRepository categoryRepository, IStickerTypeRepository typeRepository)
        {
            _categoryRepos = categoryRepository;
            _typeRepos = typeRepository;
        }
        public CategoryTypeEntity GetCategotyType()
        {
            var categories = _categoryRepos.AllCategory();
            var types = _typeRepos.AllType().Reverse();
            return new CategoryTypeEntity(types, categories);
        }
    }
}
