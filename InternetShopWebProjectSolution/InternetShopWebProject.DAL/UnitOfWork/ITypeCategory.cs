﻿using InternetShopWebProject.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetShopWebProject.DAL.UnitOfWork
{
    public interface ITypeCategory
    {
        CategoryTypeEntity GetCategotyType();
    }
}
